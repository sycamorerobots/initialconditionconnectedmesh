/**
 * 
 */
package uo.harish.prakash.sycamore.plugins.initialconditions;

import it.diunipi.volpi.sycamore.engine.Point2D;
import it.diunipi.volpi.sycamore.engine.SycamoreEngine.TYPE;
import it.diunipi.volpi.sycamore.engine.SycamoreRobotMatrix;
import it.diunipi.volpi.sycamore.gui.SycamorePanel;
import it.diunipi.volpi.sycamore.gui.SycamoreSystem;
import it.diunipi.volpi.sycamore.plugins.initialconditions.InitialConditionsImpl;
import it.diunipi.volpi.sycamore.plugins.visibilities.VisibilityImpl;
import it.diunipi.volpi.sycamore.util.ApplicationProperties;
import it.diunipi.volpi.sycamore.util.PropertyManager;
import it.diunipi.volpi.sycamore.util.SycamoreUtil;
import net.xeoh.plugins.base.annotations.PluginImplementation;

/**
 * @author harry
 *
 */
@PluginImplementation
public class InitialConditionConnectedMesh extends InitialConditionsImpl<Point2D> {

	private String	_author				= "Harish Prakash";
	private TYPE	_type				= TYPE.TYPE_2D;
	private String	_shortDescription	= "Robots on connected Mesh";
	private String	_description		= "Place robots in the viewport starting from the beginning spaced exactly n units set below";
	private String	_pluginName			= "ConnectedMesh";

	@Override
	public Point2D nextStartingPoint(SycamoreRobotMatrix<Point2D> robots) {

		float minX = PropertyManager.getSharedInstance().getIntegerProperty(ApplicationProperties.INITIAL_POSITION_MIN_X);
		float maxX = PropertyManager.getSharedInstance().getIntegerProperty(ApplicationProperties.INITIAL_POSITION_MAX_X);
		float minY = PropertyManager.getSharedInstance().getIntegerProperty(ApplicationProperties.INITIAL_POSITION_MIN_Y);
		float maxY = PropertyManager.getSharedInstance().getIntegerProperty(ApplicationProperties.INITIAL_POSITION_MAX_Y);

		float range = (VisibilityImpl.getVisibilityRange() / 2) - SycamoreSystem.getEpsilon();
		int perRow = (int) ((maxX - minX) / range) + 1;

		int robotsCount = robots.robotsCount();
		float x = minX + (robotsCount % perRow) * range;
		float y = maxY - (float) (Math.floor(robotsCount / perRow) * range);

		if (y < minY) {
			return SycamoreUtil.getRandomPoint2D(minX, maxX, minY, maxY);
		}
		else {
			return new Point2D(x, y);
		}
	}

	@Override
	public TYPE getType() {

		return _type;
	}

	@Override
	public String getAuthor() {

		return _author;
	}

	@Override
	public String getPluginShortDescription() {

		return _shortDescription;
	}

	@Override
	public String getPluginLongDescription() {

		return _description;
	}

	@Override
	public SycamorePanel getPanel_settings() {

		return null;
	}

	@Override
	public String getPluginName() {

		return _pluginName;
	}

}
